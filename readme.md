
#   Instruções para rodar o projeto

-   Tenha instalado o NodeJS na sua máquina
-   Baixe o repositório do projeto (https://bitbucket.org/GuiMaron/velasquez_vinho)
-   Usando o console vá até o diretório /dist
-   Dê o comando: *node ./index.js -e production*
-   Entre em seu navagor http://localhost:8081/

Há um pouco da documentação da API na pasta **docs**

Fiz alguns testes, apenas para demonstração, que podem ser rodados com o comando *npm run test* 
na raíz do repositório

**TODO: **fazer uma interface mais refinada para a API usando React
