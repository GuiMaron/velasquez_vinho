
const fs            = require('fs-extra')
const childProcess  = require('child_process')



try {

    // Remove current build
    fs.removeSync('./dist/')

    // Copy front-end files
    fs.copySync('./src/public', './dist/public')
    fs.copySync('./src/views', './dist/views')

    //  Copy env
    fs.copySync('./env', './dist/env')

    //  Copy MockDB
    fs.copySync('./src/daos/MockDb', './dist/daos/MockDb')

    // Transpile the typescript files (nrecisa de Typescript instalado globalmente)
    const proc = childProcess.exec('tsc --build tsconfig.prod.json')

    proc.on('close', (code) => {
        if (code !== 0) {
            throw Error('Build failed')
        }
    })

} 
catch (err) {
    console.log(err)
}
