
export const adaptCPF = (cpf: string) : string =>
{

    return (`${cpf}`.replace(/(^0+|[^\d])/gi, ''))

}
