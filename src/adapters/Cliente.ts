
import Cliente, {ClienteInterface}  from '@entities/Cliente'





export const adaptCliente = (data: any) : ClienteInterface =>
{

    return (<ClienteInterface> new Cliente(
        `${data?.nome}`
    ,   `${data?.cpf}`
    ,   data?.id
    ,   data?.compras
    ,   data?.valorTotal)
    )

}



export const adaptClientes = (data: any[]) : ClienteInterface[] =>
{

    return (<ClienteInterface[]> data.map((tuple: any) => (adaptCliente(tuple))))

}
