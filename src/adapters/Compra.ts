
import Compra, {CompraInterface}  from '@entities/Compra'



export const adaptCompra = (data: any) : CompraInterface =>
{

    return (<CompraInterface> new Compra(
        `${data?.codigo}`
    ,   `${data?.data}`
    ,   `${data?.cliente}`
    ,   data?.itens || []
    ,   Number.parseFloat(`${data?.valorTotal}`)
    ))

}



export const adaptCompras = (data: any[]) : CompraInterface[] =>
{

    return (<CompraInterface[]> data.map((tuple: any) => (adaptCompra(tuple))))

}
