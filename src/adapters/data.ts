
export const adaptData = (data: string) : Date =>
{

    let [day, month, year] = data.trim().split('-').map((fragment: string) => (Number.parseInt(`${fragment}`)))

    //  Meses iniciam em 0
    month --

    return(new Date(year, month, day))

}
