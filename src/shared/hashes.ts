
import { adaptCPF }         from '@adapters/cpf' 
import { ClienteInterface } from '@entities/Cliente'



export const hashClientesByCPF = (clientes : ClienteInterface[]) : any =>
{

    const hash : any = {}

    clientes.map((cliente: ClienteInterface) => (hash[adaptCPF(`${cliente?.cpf}`)] = cliente))

    return (hash)

}
