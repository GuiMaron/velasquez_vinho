
import './LoadEnv'

if ('development' != process.env.NODE_ENV) {
    require('module-alias/register')
}

import app      from '@server'
import logger   from '@shared/Logger'

import DEFAULTS from './defaults'





const port = Number(process.env.PORT || DEFAULTS.PORT)

app.listen(port, () => 
{
    logger.info('Express server started on port: ' + port)
})
