
import cookieParser from 'cookie-parser'
import helmet       from 'helmet'
import morgan       from 'morgan'
import path         from 'path'

import express, { Request, Response, NextFunction } from 'express'
import { BAD_REQUEST }                              from 'http-status-codes'
import 'express-async-errors'

import logger       from '@shared/Logger'

import APIRouter    from './routes/ApiRouter'

const cors = require('cors')




const app  = express()

/************************************************************************************
 *                              Set basic express settings
 ***********************************************************************************/

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cookieParser())
app.use(cors())

// Show routes called in console during development
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'))
}

// Security
if (process.env.NODE_ENV === 'production') {
    app.use(helmet())
}

// Add APIs
app.use('/api', APIRouter)

// Print API errors
app.use((error: Error, request: Request, response: Response, next: NextFunction) => 
{

    logger.error(error.message, error)

    return (response.status(BAD_REQUEST).json({
        error: error.message
    }))

})



/************************************************************************************
 *                              Serve front-end content
 ***********************************************************************************/
const viewsDir = path.join(__dirname, 'views')
app.set('views', viewsDir)

const staticDir = path.join(__dirname, 'public')
app.use(express.static(staticDir))

app.get('*', (request: Request, response: Response) => 
{
    response.sendFile('index.html', {root: viewsDir})
})



// Export express instance
export default app
