
import { ClienteInterface } from '@entities/Cliente'





const sortClienteByValorTotal = (clientes : ClienteInterface[]) : ClienteInterface[] =>
{

    return (clientes.sort((clienteA: ClienteInterface, clienteB: ClienteInterface) => 
    {
        
        const diff = (clienteB.valorTotal - clienteA.valorTotal)

        if (diff) {
            return (diff)
        }

        //  Se empatam o desempate é por menos compras (maior valor médio)
        const [ comprasA, comprasB ] = [ clienteA.compras || [], clienteB.compras || [] ]

        const comprasDiff = (comprasA.length - comprasB.length)

        if (comprasDiff) {
            return (comprasDiff)
        }

        //  Se empatam em gastos o desempate é por ordem alfabética
        return (clienteA.nome.localeCompare(clienteB.nome))

    }))

}



export default sortClienteByValorTotal
