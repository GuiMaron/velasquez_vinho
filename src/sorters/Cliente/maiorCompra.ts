
import { adaptData }        from '@adapters/data'
import { ClienteInterface } from '@entities/Cliente'





const sortClienteByMaiorCompra = (clientes : ClienteInterface[]) : ClienteInterface[] =>
{

    const sortedClientes = clientes.slice(0)

    sortedClientes.sort((clienteA: ClienteInterface, clienteB: ClienteInterface) => 
    {

        //  Pegando o valor da maior compra (que já devem estar em ordem)
        //  TODO - reordenar internamente para garantir
        const [ comprasA, comprasB ]         = [ clienteA.compras || [], clienteB.compras || [] ]
        const [ maiorCompraA, maiorCompraB ] = [ comprasA[0], comprasB[0] ]

        const comprasDiff = (maiorCompraB.valorTotal - maiorCompraA.valorTotal)

        if (comprasDiff) {
            return (comprasDiff)
        }

        //  Senão ó desempate é pela data mais atual
        const [ dataA, dataB ] = [ adaptData(maiorCompraA.data), adaptData(maiorCompraB.data) ]
        const dataDiff         = (+(dataB) - +(dataA)) 

        if (dataDiff) {
            return (dataDiff)
        }

        //  Se empatam em gastos o desempate é por ordem alfabética
        return (clienteA.nome.localeCompare(clienteB.nome))

    })

    return (sortedClientes)

}



export default sortClienteByMaiorCompra
