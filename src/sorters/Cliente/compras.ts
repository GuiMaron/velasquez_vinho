
import { adaptData }        from '@adapters/data'
import { ClienteInterface } from '@entities/Cliente'



const sortClienteByCompras = (clientes : ClienteInterface[]) : ClienteInterface[] =>
{

    return (clientes.sort((clienteA: ClienteInterface, clienteB: ClienteInterface) => 
    {

        const [ comprasA, comprasB ] = [ clienteA.compras || [],  clienteB.compras || [] ]
        const diff = (comprasB.length - comprasA.length)

        if (diff) {
            return (diff)
        }

        //  Se nenhum tem compras o desempate é alfabético
        if ((! comprasA.length) && (! comprasA.length)) {
            return (clienteA.nome.localeCompare(clienteB.nome))
        }

        if (! comprasA.length) {
            return (-1)
        }

        if (! comprasB.length) {
            return (1)
        }

        //  Capturando primeira e última compras
        const [ firstCompraA, firstCompraB ]    = [ comprasA[comprasA.length - 1], comprasB[comprasB.length - 1] ]
        const [ lastCompraA, lastCompraB ]      = [ comprasA[0], comprasB[0] ]

        //  Senão desempate é pela data mais antiga de primeira compra
        const firstCompraDiff = (+(adaptData(firstCompraA.data)) - +(adaptData(firstCompraB.data)))

        if (firstCompraDiff) {
            return (firstCompraDiff)
        }

        //  Senão desempate é pela data mais atual de última compra
        const lastCompraDiff = (+(adaptData(lastCompraB.data)) - +(adaptData(lastCompraA.data)))

        if (lastCompraDiff) {
            return (lastCompraDiff)
        }

        //  Se ainda não desempatar voltamos ao critéri oalfabético
        return (clienteA.nome.localeCompare(clienteB.nome))
        
    }))

}



export default sortClienteByCompras
