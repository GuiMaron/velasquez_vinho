
import { adaptData }        from '@adapters/data'
import { CompraInterface }  from '@entities/Compra'



const sortComprasByValorTotal = (compras : CompraInterface[], desc: boolean = true) : CompraInterface[] =>
{

    compras.sort((compraA : CompraInterface, compraB: CompraInterface) =>
    {

        const diff = ((desc) 
                   ?  ((compraB.valorTotal - compraA.valorTotal)) 
                   :  ((compraA.valorTotal - compraB.valorTotal))
        )

        if (diff) {
            return (diff)
        }

        // Critério de desempate, Compras mais atuais primeiro
        return ((desc) 
            ?   (+(adaptData(compraB.data)) - +(adaptData(compraA.data))) 
            :   (+(adaptData(compraA.data)) - +(adaptData(compraB.data)))
        )

    })

    return (compras)

}



export default sortComprasByValorTotal
