
import { adaptData }        from '@adapters/data'
import { CompraInterface }  from '@entities/Compra'
import sortComprasByData    from '@sorters/Compra/data'



const sortComprasByDataAno = (compras : CompraInterface[], desc: boolean = true) : CompraInterface[] =>
{

    return (sortComprasByData(compras, desc, true))

}



export default sortComprasByDataAno
