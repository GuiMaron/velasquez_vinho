
import { adaptData }        from '@adapters/data'
import { CompraInterface }  from '@entities/Compra'



const sortComprasByData = (compras : CompraInterface[], desc: boolean = true, onlyAno: boolean = false) : CompraInterface[] =>
{

    compras.sort((compraA : CompraInterface, compraB: CompraInterface) =>
    {

        let [ dataA, dataB ] = [ adaptData(compraA.data), adaptData(compraB.data) ]

        if (onlyAno) {
            [ dataA, dataB ] = [ new Date(dataA.getFullYear()), new Date(dataB.getFullYear()) ]
        }

        // Critério de desempate, comprar mais atuais primeiro
        return (((desc) 
            ?   (+(dataB) - +(dataA)) 
            :   (+(dataA) - +(dataB)))
        )

    })

    return (compras)

}



export default sortComprasByData
