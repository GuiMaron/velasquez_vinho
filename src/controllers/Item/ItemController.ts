
import { CompraInterface }              from '@entities/Compra'
import Controller                       from '@controllers/Controller'
import FilterParam                      from '@filters/FilterParam'

import { 
    filterProdutosByCategoria
,   filterProdutosByPais
,   filterProdutosByPreco
,   filterProdutosByProduto
,   filterProdutosBySafra
,   filterProdutosByVariedade
} from '@filters/Item/item'

import { ItemInterface, searchItem }    from '@entities/Item'

const CompraDao  = require(`@daos/Compra/CompraDao${((process.env.NODE_ENV === 'development')? ('.mock') : (''))}`).default





class ItemController extends Controller
{

    protected FILTERABLE_BY: any = {
        'produto':      filterProdutosByProduto
    ,   'variedade':    filterProdutosByVariedade
    ,   'pais':         filterProdutosByPais
    ,   'categoria':    filterProdutosByCategoria
    ,   'safra':        filterProdutosBySafra
    ,   'preco':        filterProdutosByPreco
    }



    public async listAll (filterBy?: FilterParam[]) : Promise<ItemInterface[]> 
    {

        try {

            const compraDao = new CompraDao()

            const compras   = await compraDao.listAll()

            let items: ItemInterface[] = []

            compras.map((compra: CompraInterface) => 
            {

                compra.itens.map((item: ItemInterface) => 
                {

                    let index = searchItem(item, items)

                    if (-1 === index) {
                        items.push(item)
                    }
                    else {
                        
                        //  Se o item que está na lista não tem código e o da compra tem, colocar no da list
                        if ((! items[index].codigo) && (item.codigo)) {
                            items[index].codigo = item.codigo
                        }

                    }

                })

            })

            if (filterBy) {
                items = this.filter(items, filterBy)
            }

            return (items)

        } 
        catch (error) {
            throw (error)
        }  

    }

}



export default ItemController
