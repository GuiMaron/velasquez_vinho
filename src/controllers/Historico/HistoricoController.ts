
import { adaptCPF }             from '@adapters/cpf'
import { adaptData }            from '@adapters/data'
import { CompraInterface }      from '@entities/Compra'
import Controller               from '@controllers/Controller'
import filterHistoricoByCliente from '@filters/Historico/cliente'
import FilterParam              from '@filters/FilterParam'
import Historico, { addHistorico, HistoricoInterface, sortHistoricos } from '@entities/Historico'
import { ItemInterface }        from '@entities/Item'

const CompraDao  = require(`@daos/Compra/CompraDao${((process.env.NODE_ENV === 'development') ? ('.mock') : (''))}`).default





class HistoricoController extends Controller
{

    protected FILTERABLE_BY: any = {
        'cliente':  filterHistoricoByCliente
    }



    public async listAll (filterBy?: FilterParam[]) : Promise<HistoricoInterface[]> 
    {

        try {

            const compraDao           = new CompraDao()

            const compras             = await compraDao.listAll()
            const historicosHash: any = {}

            compras.map((compra: CompraInterface) => 
            {

                const cliente = adaptCPF(compra.cliente)
                
                if (! historicosHash[cliente]) {
                    historicosHash[cliente] = new Historico(cliente)
                }

                const historico = historicosHash[cliente]

                compra.itens.map((item: ItemInterface) => 
                {

                    //  Histórico de produtos (por codigo)
                    if (item.codigo) {
                        addHistorico(item, historico, 'produto', 'codigo',  item.codigo, { produto: item.produto })
                    }
                    else {
                        addHistorico(item, historico, 'produto', 'produto', item.produto, { codigo: null })
                    }

                    //  Variedade
                    if (item.variedade) {
                        addHistorico(item, historico, 'variedade', 'variedade', item.variedade)
                    }

                    //  País
                    if (item.pais) {
                        addHistorico(item, historico, 'pais', 'pais', item.pais)
                    }

                    //  Categoria
                    if (item.categoria) {
                        addHistorico(item, historico, 'categoria', 'categoria', item.categoria)
                    }

                    //  Safra
                    if (item.safra) {
                        addHistorico(item, historico, 'safra', 'safra', item.safra)
                    }

                })

                if (! historico.ultimaCompra) {
                    historico.ultimaCompra = compra
                }

                //  Apenas troca se data da compra for mais atual
                if (+(adaptData(historico.ultimaCompra.data)) < +(adaptData(compra.data))) {
                    historico.ultimaCompra = compra
                }


            })

            //  Ordenados
            let historicos: HistoricoInterface[] = sortHistoricos(<HistoricoInterface[]> Object.values(historicosHash))

            if (filterBy) {
                historicos = this.filter(historicos, filterBy)
            }

            return (historicos)

        } 
        catch (error) {
            throw (error)
        }  

    }

}



export default HistoricoController
