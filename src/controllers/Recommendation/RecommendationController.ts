
import Controller                       from '@controllers/Controller'
import FilterParam                      from '@filters/FilterParam'

import { 
    filterProdutosByCategoria
,   filterProdutosByPais
,   filterProdutosByPreco
,   filterProdutosByProduto
,   filterProdutosBySafra
,   filterProdutosByVariedade
} from '@filters/Item/item'

import HistoricoController  from '@controllers/Historico/HistoricoController'
import ItemController       from '@controllers/Item/ItemController'
import { ItemInterface }    from '@entities/Item'



class RecommendationController extends Controller
{

    public async listAll () : Promise<ItemInterface[]> 
    {

        const itemController    = new ItemController()
        const items             = await itemController.listAll()
        return (items)

    }



    public async recommend (cliente: string, type: string) : Promise<ItemInterface> 
    {

        const historicoController   = new HistoricoController()
        const itemController        = new ItemController()

        const filterParams: FilterParam[] = [ { attribute: 'cliente', value: cliente } ]

        //  Aqui filtramos apenas o histórico desse cliente
        const historico = await (await historicoController.listAll(filterParams)).shift()

        //  Pegar o item no topo do tipo de historico
        const topInType = (<any> historico)[type][0]
        const topValue  = (<any> topInType)[type]
        
        // E itens filtrados pelo tipo (mas apenas o primeiro)
        const item = await (await itemController.listAll([ { attribute: type, value: topValue } ])).shift()

        //  Retornando o item recomendado
        return (<ItemInterface> item)

    }

}



export default RecommendationController
