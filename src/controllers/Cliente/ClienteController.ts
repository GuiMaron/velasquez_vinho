
import { adaptClientes }                from '@adapters/Cliente'
import { ClienteInterface }             from '@entities/Cliente'   
import Controller                       from '@controllers/Controller'
import { enrichClientesWithCompras }    from '@enrichers/Cliente'
import EnricherParam                    from '@enrichers/EnricherParam'
import filterClienteswithCompras        from '@filters/Cliente/withCompras'
import FilterParam                      from '@filters/FilterParam'
import SorterParam                      from '@sorters/SorterParam'
import sortClienteByValorTotal          from '@sorters/Cliente/valortotal'
import sortClienteByCompras             from '@sorters/Cliente/compras'

const ClienteDao = require(`@daos/Cliente/ClienteDao${((process.env.NODE_ENV === 'development') ? ('.mock') : (''))}`).default





class ClienteController extends Controller
{

    protected ENRICHABLE_BY: any = {
        'compras':      enrichClientesWithCompras
    }

    protected FILTERABLE_BY: any = {
        'compras':      filterClienteswithCompras
    }

    protected SORTABLE_BY:   any = {
        'compras':      sortClienteByCompras
    ,   'valorTotal':   sortClienteByValorTotal
    }



    public async listAll (enrichedBy?: EnricherParam[], sortBy?: SorterParam[], filterBy?: FilterParam[]) : Promise<ClienteInterface[]> 
    {

        try {

            const clienteDao    = new ClienteDao()

            let clientes = await clienteDao.listAll()
            clientes     = adaptClientes(clientes)

            if (enrichedBy) {
                clientes = this.enrich(clientes, enrichedBy)
            }

            if (sortBy) {
                clientes = this.sort(clientes, sortBy)
            }

            if (filterBy) {
                clientes = this.filter(clientes, filterBy)
            }

            return (clientes)

        } 
        catch (error) {
            throw (error)
        }

    }

}



export default ClienteController
