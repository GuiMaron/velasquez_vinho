
import EnricherParam    from '@enrichers/EnricherParam'
import FilterParam      from '@filters/FilterParam'
import SorterParam      from '@sorters/SorterParam'
import splitEntitites   from '@entities/splitEntitities'





abstract class Controller
{

    protected ENRICHABLE_BY: any = {}
    protected FILTERABLE_BY: any = {}
    protected SORTABLE_BY:   any = {}





    public enrich (entities: any[], params: EnricherParam[]) : any[]
    {

        while (params.length) {

            const param = (<EnricherParam> params.shift())

            if (! this.ENRICHABLE_BY[param.attribute]) {
                throw (new Error(`Invalid enricher param: ${param.attribute}`))
            }

            entities = this.ENRICHABLE_BY[param.attribute].call(null, entities, param.entities)

        }

        return (<any[]> entities)

    }



    public filter (entities: any[], params: FilterParam[]) : any[]
    {

        try {

            while (params.length) {

                const param = (<FilterParam> params.shift())
                
                if (! this.FILTERABLE_BY[param.attribute]) {
                    throw (new Error(`Invalid filter param: ${param.attribute}`))
                }

                entities = this.FILTERABLE_BY[param.attribute].call(null, entities, param.value)

                let splittedEntitites = splitEntitites(entities, param.attribute)

                splittedEntitites = splittedEntitites.map((split: any[]) => (this.sort(split.slice(0), params.slice(0))))
                entities          = []

                splittedEntitites.map((orderedSplit: any[]) => (entities = entities.concat(orderedSplit)))

            }

            return (<any[]> entities)

        }
        catch(error) {
            throw (error) 
        }

    }



    public abstract async listAll (enrichedBy?: EnricherParam[], sortBy?: SorterParam[], filterBy?: FilterParam[]) : Promise<any[]> 



    public sort (entities: any[], params: SorterParam[]) : any[]
    {

        try {

            if (params.length) {

                const param = (<SorterParam> params.shift())

                if (! this.SORTABLE_BY[param.attribute]) {
                    throw (new Error(`Invalid sort param: ${param.attribute}`))
                }

                entities = this.SORTABLE_BY[param.attribute].call(null, entities, param.desc)

                let splittedEntitites = splitEntitites(entities, param.attribute)

                splittedEntitites = splittedEntitites.map((split: any[]) => (this.sort(split.slice(0), params.slice(0))))
                entities          = []

                splittedEntitites.map((orderedSplit: any[]) => (entities = entities.concat(orderedSplit)))

            }

            return (<any[]> entities)

        }
        catch(error) {
            throw (error) 
        }

    }

}



export default Controller
