
import { adaptCompras }         from '@adapters/Compra'
import { CompraInterface }      from '@entities/Compra'   
import Controller               from '@controllers/Controller'
import filterComprasByCliente   from '@filters/Compra/cliente'
import filterComprasInAno       from '@filters/Compra/inAno'
import FilterParam              from '@filters/FilterParam'
import sortComprasByData        from '@sorters/Compra/data'
import sortComprasByDataAno     from '@sorters/Compra/dataAno'
import sortComprasByValorTotal  from '@sorters/Compra/valorTotal'
import SorterParam              from '@sorters/SorterParam'

const CompraDao = require(`@daos/Compra/CompraDao${((process.env.NODE_ENV === 'development')? ('.mock') : (''))}`).default





class CompraController extends Controller
{

    protected FILTERABLE_BY: any = {
        'cliente':      filterComprasByCliente
    ,   'data-ano':     filterComprasInAno
    }

    protected SORTABLE_BY:   any = {
        'data':         sortComprasByData
    ,   'data-ano':     sortComprasByDataAno
    ,   'valorTotal':   sortComprasByValorTotal
    }



    public async listAll (sortBy?: SorterParam[], filterBy?: FilterParam[]) : Promise<CompraInterface[]> 
    {

        try {

            const compraDao = new CompraDao()

            let compras     = await compraDao.listAll()
            compras         = adaptCompras(compras)

            if (sortBy) {
                compras = this.sort(compras, sortBy)
            }

            if (filterBy) {
                compras = this.filter(compras, filterBy)
            }

            return (compras)

        } 
        catch (error) {
            throw (error)
        }

    }

}



export default CompraController
