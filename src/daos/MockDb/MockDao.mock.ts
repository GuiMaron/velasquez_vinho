
import jsonfile from 'jsonfile'



export class MockDaoMock 
{

    private readonly clientesDbFilePath = `${((process.env.NODE_ENV === 'production') ? ('.') : ('src'))}/daos/MockDb/ClientesDb.json`
    private readonly comprasDbFilePath  = `${((process.env.NODE_ENV === 'production') ? ('.') : ('src'))}/daos/MockDb/ComprasDb.json`



    protected listClientes () : Promise<any> 
    {
        return (jsonfile.readFile(this.clientesDbFilePath))
    }

    protected listCompras () : Promise<any> 
    {
        return (jsonfile.readFile(this.comprasDbFilePath))
    }

}
