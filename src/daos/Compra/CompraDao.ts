
import { CompraInterface }  from '@entities/Compra'
import { MockDaoMock }      from '@daos/MockDb/MockDao.mock'



export interface CompraDaoInterface 
{
    listAll:    () => Promise<CompraInterface[]>
}





class CompraDao extends     MockDaoMock 
                implements  CompraDaoInterface 
{

    public async listAll () : Promise<CompraInterface[]> 
    {

        try {

            const allCompras = await super.listCompras()
            return (allCompras)

        } 
        catch (error) {
            throw (error)
        }

    }

}



export default CompraDao
