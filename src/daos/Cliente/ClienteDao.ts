
import { ClienteInterface } from '@entities/Cliente'
import { MockDaoMock }      from '@daos/MockDb/MockDao.mock'



export interface ClienteDaoInterface 
{
    listAll: () => Promise<ClienteInterface[]>
}





export class ClienteDao extends     MockDaoMock 
                        implements  ClienteDaoInterface 
{

    public async listAll () : Promise<ClienteInterface[]> 
    {

        try {

            const clientes = await super.listClientes()
        
            return (clientes)

        } 
        catch (error) {
            throw (error)
        }

    }

}



export default ClienteDao
