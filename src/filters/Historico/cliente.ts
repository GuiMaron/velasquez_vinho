
import { adaptCPF }             from '@adapters/cpf'
import { HistoricoInterface }   from '@entities/Historico'





const filterHistoricoByCliente = (historicos: HistoricoInterface[], cliente: string) => 
{

    return (historicos.filter((historico: HistoricoInterface) => (historico.cliente === adaptCPF(cliente))))

}



export default filterHistoricoByCliente
