
export default interface FilterParam 
{
    attribute:  string
    value?:     any
    //  valueA?:    any (TODO: primeiro valor, para, por exemplo, um between)
    //  valueB?:    any (TODO: segundo valor, para, por exemplo, um between)
    //  type?:      any (TODO: um enum de operações que chamam callbacks, para poder fazer várias comparações)
}
