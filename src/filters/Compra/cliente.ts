
import { adaptCPF }         from '@adapters/cpf'
import { CompraInterface }  from '@entities/Compra'





const filterComprasByCliente = (compras: CompraInterface[], cliente: string) => 
{
    
    return (compras.filter((compra: CompraInterface) => ((adaptCPF(compra.cliente) === adaptCPF(cliente)))))

}



export default filterComprasByCliente
