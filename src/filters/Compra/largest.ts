
import { adaptData }        from '@adapters/data'
import { ClienteInterface } from '@entities/Cliente'
import { CompraInterface }  from '@entities/Compra'





const filterLargestCompra = (clientes: ClienteInterface[], year: string) => 
{
    
    return (clientes.filter((cliente: ClienteInterface) => 
    {

        const compras         = cliente.compras || []

        const hasCompraInYear = compras.reduce((hasCompra: boolean, compra: CompraInterface) => 
            (hasCompra || (`${adaptData(compra.data).getFullYear()}` === year))
        ,   false
        )
        
        return (hasCompraInYear)

    }))

}



export default filterLargestCompra
