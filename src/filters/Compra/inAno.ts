
import { adaptData }        from '@adapters/data'
import { CompraInterface }  from '@entities/Compra'





const filterComprasInAno = (compras: CompraInterface[], year: string) => 
{
    
    return (compras.filter((compra: CompraInterface) => ((`${adaptData(compra.data).getFullYear()}` === year))))

}



export default filterComprasInAno
