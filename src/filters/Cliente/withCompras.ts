
import { ClienteInterface } from '@entities/Cliente'



const filterClienteswithCompras = (clientes: ClienteInterface[]) => 
{
    
    return (clientes.filter((cliente: ClienteInterface) => 
    {

        const compras = cliente.compras || []
        
        return (compras.length)

    }))

}



export default filterClienteswithCompras
