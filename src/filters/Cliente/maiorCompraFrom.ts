
import { ClienteInterface } from '@entities/Cliente'



const filterMaiorCompraFromCliente = (clientes: ClienteInterface[]) => 
{
    
    return (clientes.map((cliente: ClienteInterface) => 
    {

        const compras   = cliente.compras || []
        cliente.compras = [compras[0]]    || []

        return (cliente)

    }))

}



export default filterMaiorCompraFromCliente
