
import { ItemInterface }   from '@entities/Item'





const filterItemByAttribute = (items: ItemInterface[], value: any, attribute: string) : ItemInterface[] => 
{

    return (items.filter((item: ItemInterface) => (<any> item)[attribute] == value))

}



export const filterProdutosByCategoria = (items: ItemInterface[], value: any) : ItemInterface[] => 
{
    return (filterItemByAttribute(items, value, 'categoria'))
}

export const filterProdutosByPais = (items: ItemInterface[], value: any) : ItemInterface[] => 
{
    return (filterItemByAttribute(items, value, 'pais'))
}

export const filterProdutosByPreco = (items: ItemInterface[], value: any) : ItemInterface[] => 
{
    return (filterItemByAttribute(items, value, 'preco'))
}

export const filterProdutosByProduto = (items: ItemInterface[], value: any) : ItemInterface[] => 
{
    return (filterItemByAttribute(items, value, 'produto'))
}

export const filterProdutosBySafra = (items: ItemInterface[], value: any) : ItemInterface[] => 
{
    return (filterItemByAttribute(items, value, 'safra'))
}

export const filterProdutosByVariedade = (items: ItemInterface[], value: any) : ItemInterface[] => 
{
    return (filterItemByAttribute(items, value, 'variedade'))
}
