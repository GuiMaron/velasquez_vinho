
import { adaptCPF }             from '@adapters/cpf'
import { ClienteInterface }     from '@entities/Cliente'
import { CompraInterface }      from '@entities/Compra'
import { hashClientesByCPF }    from '@shared/hashes'
import sortComprasByData        from '@sorters/Compra/data' 



export const enrichClientesWithCompras = (clientes: ClienteInterface[], compras: CompraInterface[]) =>
{

    const hashClientes = hashClientesByCPF(clientes)

    //  Adiciona as compras ao cliente
    compras.map((compra: CompraInterface) => 
    {

        const cpf = adaptCPF(`${compra?.cliente}`)

        if (hashClientes[cpf]) {

            const cliente : ClienteInterface = (<ClienteInterface> hashClientes[cpf])
            
            cliente.compras?.push(compra)
            cliente.valorTotal += compra.valorTotal

        }

    })

    //  Ordena as compras em cada cliente (pela data)
    //  Aproveito e já boto uma precisão fixa no total de compras
    clientes.map((cliente: ClienteInterface) => 
    {

        cliente.valorTotal = Number.parseFloat(Number(cliente.valorTotal).toFixed(2))
        
    })

    return (clientes)

}
