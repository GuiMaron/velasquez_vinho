
const splitEntitites = (tuples: any[], attribute: string) : any[] =>
{

    const splits: any = []

    if (! tuples) {
        return (splits)
    }

    tuples.map((tuple: any, index: number) => 
    {

        if ((index === 0) 
        ||  ((<any>tuple)[attribute] !== (<any>tuples[index - 1])[attribute])) {

            splits[splits.length] = []

        }

        splits[splits.length - 1].push(tuple)

    })

    return (<any[]> splits)

}



export default splitEntitites
