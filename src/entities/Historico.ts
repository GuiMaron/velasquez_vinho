
import Compra, { CompraInterface }  from '@entities/Compra'
import { ItemInterface }            from '@entities/Item'





export interface ProdutoHistoricoInterface      { produto: string,      codigo?: string,   quantidade: number }
export interface VariedadeHistoricoInterface    { variedade: string,    quantidade: number }
export interface PaisHistoricoInterface         { pais: string,         quantidade: number }
export interface CategoriaHistoricoInterface    { categoria: string,    quantidade: number }
export interface SafraHistoricoInterface        { safra: number,        quantidade: number }

export interface HistoricoInterface 
{
    cliente:        string
    produto:        ProdutoHistoricoInterface[]
    variedade:      VariedadeHistoricoInterface[]
    pais:           PaisHistoricoInterface[]
    categoria:      CategoriaHistoricoInterface[]
    safra:          SafraHistoricoInterface[]
    ultimaCompra:   (CompraInterface | null)
}



class Historico implements HistoricoInterface 
{

    public cliente:         string
    public produto:         ProdutoHistoricoInterface[]
    public variedade:       VariedadeHistoricoInterface[]
    public pais:            PaisHistoricoInterface[]
    public categoria:       CategoriaHistoricoInterface[]
    public safra:           SafraHistoricoInterface[]
    public ultimaCompra:    (CompraInterface | null)



    constructor (
        clienteOuHistorico: (string | HistoricoInterface)            
    ,   produto:            ProdutoHistoricoInterface[]     = []
    ,   variedade:          VariedadeHistoricoInterface[]   = []
    ,   pais:               PaisHistoricoInterface[]        = []
    ,   categoria:          CategoriaHistoricoInterface[]   = []
    ,   safra:              SafraHistoricoInterface[]       = []
    ,   ultimaCompra:       (CompraInterface | null)        = null
    ) 
    {

        if ('string' === typeof(clienteOuHistorico)) {

            this.cliente        = `${clienteOuHistorico}`
            this.produto        = produto
            this.variedade      = variedade
            this.pais           = pais
            this.categoria      = categoria
            this.safra          = safra
            this.ultimaCompra   = ultimaCompra
    
        }
        else {

            this.cliente        = clienteOuHistorico.cliente
            this.produto        = clienteOuHistorico.produto
            this.variedade      = clienteOuHistorico.variedade
            this.pais           = clienteOuHistorico.pais
            this.categoria      = clienteOuHistorico.categoria
            this.safra          = clienteOuHistorico.safra
            this.ultimaCompra   = clienteOuHistorico.ultimaCompra

        }

    }

}



export const  addHistorico = (
    item:       ItemInterface
,   historico:  HistoricoInterface
,   type:       string
,   attribute:  string
,   value:      any
,   extraData?: any) 
: void =>
{
    
    let index = searchHistorico(historico, type, attribute, (<any> item)[attribute])

    if (-1 === index) {

        let tuple: any       = { quantidade: 1 }
            tuple[attribute] = value

        if (extraData) {
            tuple = Object.assign(tuple, extraData)
        }

        (<any> historico)[type].push(tuple)

    }
    else {
        (<any> historico)[type][index].quantidade ++
    }

}



export const searchHistorico = (
    historico:  HistoricoInterface
,   type:       string
,   attribute:  string
,   value:      any
) 
: number => 
{

    let index = -1

    if (! (<any> historico)[type]) {
        return (index)
    }

    const tuples = (<any> historico)[type]

    for (let index in tuples) {

        let tuple = tuples[index]

        if (tuple[attribute] === value) {
            return (Number.parseInt(index))
        }

    }

    return (index)

}



export const sortHistorico = (historico: HistoricoInterface) : HistoricoInterface => 
{

    Object.keys(historico).map((type: string) => 
    {
        (<any> historico)[type] = sortTipoHistorico((<any> historico)[type])
    })

    return (historico)

}



export const sortHistoricos = (historicos: HistoricoInterface[]) : HistoricoInterface[] => 
{

    return (historicos.map((historico: HistoricoInterface) => sortHistorico(historico)))

}



const sortTipoHistorico = (tipoHistorico: any[]) : any[] => 
{

    if (! Array.isArray(tipoHistorico)) {
        return (tipoHistorico)
    }

    tipoHistorico.sort((itemA: any, itemB: any) => (itemB.quantidade - itemB.quantidade))

    return (tipoHistorico)

}




export default Historico
