
import { ClienteInterface } from '@entities/Cliente'
import { ItemInterface }    from '@entities//Item'



export interface CompraInterface 
{
    codigo:     string
    data:       string
    cliente:    string
    itens:      ItemInterface[]
    valorTotal: number
}



class Compra implements CompraInterface 
{

    public codigo:      string
    public data:        string
    public cliente:     string
    public itens:       ItemInterface[]
    public valorTotal:  number



    constructor (
        codigoOuCompra: (string | CompraInterface)
    ,   data?:          string
    ,   cliente?:       (string |ClienteInterface)
    ,   itens:          ItemInterface[] = []
    ,   valorTotal:     number          = NaN
    )
    {

        if ('string' === typeof(codigoOuCompra)) {

            [ 
                this.codigo
            ,   this.data
            ,   this.cliente
            ,   this.itens
            ,   this.valorTotal
            ]
            =
            [
                `${codigoOuCompra}`
            ,   `${data}`
            ,   `${(('string' !== typeof(cliente)) ? (`${(<ClienteInterface> cliente)?.nome}`) : (`${cliente}`))}`
            ,   (<ItemInterface[]> itens)
            ,   Number.parseFloat(`${valorTotal}`)
            ]

        } 
        else {
            
            [ 
                this.codigo
            ,   this.data
            ,   this.cliente
            ,   this.itens
            ,   this.valorTotal
            ]
            =
            [
                codigoOuCompra.codigo
            ,   codigoOuCompra.data
            ,   codigoOuCompra.cliente
            ,   codigoOuCompra.itens
            ,   codigoOuCompra.valorTotal
            ]

        }

    }

}



export default Compra
