
import { CompraInterface } from '@entities/Compra'



export interface ClienteInterface 
{
    id:         number
    nome:       string
    cpf:        string
    compras?:   CompraInterface[]
    valorTotal: number
}



class Cliente implements ClienteInterface 
{

    public id:          number
    public nome:        string
    public cpf:         string
    public compras?:    CompraInterface[] = []
    public valorTotal:  number



    constructor (
        nomeOuCliente:  (string | ClienteInterface)
    ,   cpf:            string            = ''
    ,   id:             number            = -1
    ,   compras:        CompraInterface[] = []
    ,   valorTotal:     number            = 0
    ) 
    {

        if ('string' === typeof(nomeOuCliente)) {

            this.compras    = compras
            this.cpf        = `${cpf}`
            this.id         = id
            this.nome       = `${nomeOuCliente}`
            this.valorTotal = valorTotal

        } 
        else {

            this.compras    = nomeOuCliente.compras 
            this.cpf        = nomeOuCliente.cpf
            this.id         = nomeOuCliente.id
            this.nome       = nomeOuCliente.nome
            this.valorTotal = nomeOuCliente.valorTotal
   
        }

    }

}



export default Cliente
