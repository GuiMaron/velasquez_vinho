
export interface ItemInterface 
{
    produto:    string
    codigo:     string
    variedade:  string
    pais:       string
    categoria:  string
    safra:      string
    preco:      number
}



class Item implements ItemInterface 
{

    public produto:     string
    public codigo:      string
    public variedade:   string
    public pais:        string
    public categoria:   string
    public safra:       string
    public preco:       number



    constructor (
        produtoOuItem:  string | ItemInterface
    ,   codigo?:        string
    ,   variedade?:     string
    ,   pais?:          string
    ,   categoria?:     string
    ,   safra?:         string
    ,   preco?:         number   
    ) 
    {

        if ('string' === typeof(produtoOuItem)) {

            this.produto    = `${produtoOuItem}`
            this.codigo     = `${codigo}`
            this.variedade  = `${variedade}`
            this.pais       = `${pais}`
            this.categoria  = `${categoria}`
            this.safra      = `${safra}` 
            this.preco      =  Number.parseFloat(`${preco}`)

        } 
        else {
            
            this.produto    = produtoOuItem.produto
            this.codigo     = produtoOuItem.codigo
            this.variedade  = produtoOuItem.variedade
            this.pais       = produtoOuItem.pais
            this.categoria  = produtoOuItem.categoria
            this.safra      = produtoOuItem.safra 
            this.preco      = produtoOuItem.preco

        }

    }

}



export const compareItems = (itemA: ItemInterface, itemB: ItemInterface) : boolean =>
{

    return (
        (itemA.produto      === itemB.produto) 
    &&  (itemA.variedade    === itemB.variedade) 
    &&  (itemA.pais         === itemB.pais)
    &&  (itemA.categoria    === itemB.categoria)
    &&  (itemA.safra        === itemB.safra)
    )

}



export const searchItem = (item : ItemInterface, items : ItemInterface[]) : number =>
{

    let index = -1

    for (let index in items) {

        const currentItem = items[index]

        if (compareItems(currentItem, item)) {
            return (Number.parseInt(index))
        }

    }

    return (index)

}



export default Item
