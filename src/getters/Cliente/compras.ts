
import { CompraInterface }  from '@entities/Compra'
import ComprasController    from '@controllers/Compra/CompraController'
import FilterParam          from '@filters/FilterParam'
import SorterParam          from '@sorters/SorterParam'





export const getComprasForClientes = async (sortBy?: SorterParam[], filterBy?: FilterParam[]) : Promise<CompraInterface[]> =>
{

    try {

        const comprasController = new ComprasController()

        const sorterParams: SorterParam[] = sortBy || [ { attribute: 'data' } ]
        const compras                     = await comprasController.listAll(sorterParams, filterBy)

        return (compras)

    }
    catch (error) {
        throw (error)
    }

}



export const getComprasFromAnoForClientes = async (ano?: string, sortBy?: SorterParam[], filterBy?: FilterParam[]) : Promise<CompraInterface[]> =>
{

    if (! ano) {
        ano = `${new Date().getFullYear()}`
    }

    try {

        const comprasController = new ComprasController()

        const sorterParams: SorterParam[] = [{ attribute: 'data-ano' }, { attribute: 'valorTotal' }]
        const filterParams: FilterParam[] = [{ attribute:  'data-ano', value: ano } ]
        const compras                     = await comprasController.listAll(sorterParams, filterParams)

        return (compras)

    }
    catch (error) {
        throw (error)
    }

}



export default getComprasForClientes
