
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import ItemController from '@controllers/Item/ItemController'





const ItemListRouter = Router()


/******************************************************************************
 *                      LIST Item - "GET /api/item/list"
 ******************************************************************************/
ItemListRouter.get('*', async (request: Request, response: Response) => 
{
    
    try {

        const itemController = new ItemController()
        const items          = await itemController.listAll()

        return (response.status(OK).json({items}))

    }
    catch (error) {
        throw (error)
    }

})



export default ItemListRouter
