
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import ItemController from '@controllers/Item/ItemController'





const ItemFIlterRouter = Router()


/******************************************************************************
 *                      LIST Item - "GET /api/item/filter/:attribute/:value"
 ******************************************************************************/
ItemFIlterRouter.get('/:attribute/:value*', async (request: Request, response: Response) => 
{
    
    try {

        const itemController = new ItemController()
        const items          = await itemController.listAll([{ attribute: request.params.attribute, value: request.params.value }])

        return (response.status(OK).json({items}))

    }
    catch (error) {
        throw (error)
    }

})



export default ItemFIlterRouter
