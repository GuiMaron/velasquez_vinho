
import { Router } from 'express'

import FilterRouter from './filter/CompraFilterRouter'
import ListRouter   from './list/CompraListRouter'
import SortRouter   from './sort/CompraSortRouter'





const CompraRouter = Router()

CompraRouter.use('/filter', FilterRouter)
CompraRouter.use('/sort',   SortRouter)
CompraRouter.use('',        ListRouter)



export default CompraRouter
