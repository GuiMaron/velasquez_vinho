
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import CompraController from '@controllers/Compra/CompraController'
import FilterParam      from '@filters/FilterParam'
import SorterParam      from '@sorters/SorterParam'





const CompraFilterRouter = Router()


/******************************************************************************
 *                      FILTER Compras por ano - "GET /api/compra/filter/data-ano/:ano"
 ******************************************************************************/
CompraFilterRouter.get('/data-ano/:ano*', async (request: Request, response: Response) => 
{

    try {

        try {

            const compraController = new CompraController()
    
            const sorterParams: SorterParam[] = [ { attribute: 'valorTotal' } ]
            const filterParams: FilterParam[] = [ { attribute: 'data-ano', value: request.params.ano } ]
            const compras                     = await compraController.listAll(sorterParams, filterParams)
    
            return (response.status(OK).json({compras}))
    
        }
        catch (error) {
            throw (error)
        }

    }
    catch (error) {
        throw (error)
    }

})



/******************************************************************************
 *                      FILTER Compras por cliente - "GET /api/compra/filter/cliente/:cliente"
 ******************************************************************************/
CompraFilterRouter.get('/cliente/:cliente*', async (request: Request, response: Response) => 
{

    try {

        try {

            const compraController = new CompraController()
    
            const filterParams: FilterParam[] = [ { attribute: 'cliente', value: request.params.cliente } ]
            const compras                     = await compraController.listAll(undefined, filterParams)
    
            return (response.status(OK).json({compras}))
    
        }
        catch (error) {
            throw (error)
        }

    }
    catch (error) {
        throw (error)
    }

})



export default CompraFilterRouter
