
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import CompraController from '@controllers/Compra/CompraController'





const CompraListRouter = Router()


/******************************************************************************
 *                      LIST Compras - "GET /api/compra/list"
 ******************************************************************************/
CompraListRouter.get('*', async (request: Request, response: Response) => 
{
    
    try {

        const compraController = new CompraController()
        const compras          = await compraController.listAll()

        return (response.status(OK).json({compras}))

    }
    catch (error) {
        throw (error)
    }

})



export default CompraListRouter
