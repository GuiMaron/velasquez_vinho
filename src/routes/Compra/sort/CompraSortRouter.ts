
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import CompraController from '@controllers/Compra/CompraController'
import SorterParam      from '@sorters/SorterParam'





const CompraSortRouter = Router()

/******************************************************************************
 *                      SORT Compras - "GET /api/compra/sort/data"
 ******************************************************************************/
CompraSortRouter.get('/data*', async (request: Request, response: Response) => 
{
    
    try {

        const compraController = new CompraController()

        const sorterParams: SorterParam[] = [ { attribute: 'data' } ]
        const compras                     = await compraController.listAll(sorterParams)

        return (response.status(OK).json({compras}))

    }
    catch (error) {
        throw (error)
    }

})



/******************************************************************************
 *                      SORT Compras - "GET /api/compra/sort/valorTotal"
 ******************************************************************************/
CompraSortRouter.get('/valorTotal*', async (request: Request, response: Response) => 
{
    
    try {

        const compraController = new CompraController()

        const sorterParams: SorterParam[] = [ { attribute: 'valorTotal' } ]
        const compras                     = await compraController.listAll(sorterParams)

        return (response.status(OK).json({compras}))

    }
    catch (error) {
        throw (error)
    }

})



export default CompraSortRouter
