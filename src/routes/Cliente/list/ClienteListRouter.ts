
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import ClienteController from '@controllers/Cliente/ClienteController'





const ClienteListRouter = Router()


/******************************************************************************
 *                      LIST Clientes - "GET /api/cliente/list"
 ******************************************************************************/
ClienteListRouter.get('*', async (request: Request, response: Response) => 
{
    
    try {

        const clienteController = new ClienteController()
        const clientes          = await clienteController.listAll()

        return (response.status(OK).json({clientes}))

    }
    catch (error) {
        throw (error)
    }

})



export default ClienteListRouter
