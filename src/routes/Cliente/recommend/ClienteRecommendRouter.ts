
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import RecommendationController from '@controllers/Recommendation/RecommendationController'
import FilterParam              from '@filters/FilterParam'





const ClienteRecommendRouter = Router()

/******************************************************************************
 *                      RECOMMEND Type to Cliente - "GET /api/cliente/recommend/:cliente/:type"
 ******************************************************************************/
//  # 4 - Recomende um vinho para um determinado cliente a partir do histórico de compras.
ClienteRecommendRouter.get('/:cliente/:type*', async (request: Request, response: Response) => 
{

    try {

        const recommendationController = new RecommendationController()
    
        const recommendation = await recommendationController.recommend(request.params.cliente, request.params.type)

        return (response.status(OK).json({recommendation}))

    }
    catch (error) {
        throw (error)
    }

})



export default ClienteRecommendRouter
