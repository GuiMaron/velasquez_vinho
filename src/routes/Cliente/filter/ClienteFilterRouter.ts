
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import ClienteController                from '@controllers/Cliente/ClienteController'
import { ClienteInterface }             from '@entities/Cliente'
import EnricherParam                    from '@enrichers/EnricherParam'
import filterMaiorCompraFromCliente     from '@filters/Cliente/maiorCompraFrom'
import FilterParam                      from '@filters/FilterParam'
import { getComprasFromAnoForClientes } from '@getters/Cliente/compras'
import sortClienteByMaiorCompra         from '@sorters/Cliente/maiorCompra'
import SorterParam                      from '@sorters/SorterParam'





const ClienteFilterRouter = Router()

/******************************************************************************
 *                      FILTER Clientes (1) pela maior compra no ano - "GET /api/cliente/filter/maior-compra-ano/:ano"
 ******************************************************************************/
//  # 2 - Mostre o cliente com maior compra única no último ano (2016).
ClienteFilterRouter.get('/maior-compra-ano/:ano*', async (request: Request, response: Response) => 
{

    try {

        const clienteController = new ClienteController()

        //  Aqui obtemos apenas as compras do ano, em ordem de valor
        const compras           = await getComprasFromAnoForClientes(request.params.ano)

        const enricherParams: EnricherParam[]   = [ { attribute: 'compras', entities: compras } ]
        const sorterParams:   SorterParam[]     = [ { attribute: 'valorTotal' } ]
        const filterParams:   FilterParam[]     = [ { attribute: 'compras'    } ]

        //  Aqui filtramos apenas a maior compra de cada cliente
        let clientes = await clienteController.listAll(enricherParams, sorterParams, filterParams)
            clientes = filterMaiorCompraFromCliente(clientes)

        clientes = sortClienteByMaiorCompra(clientes)
        
        //  Apenas o TOP 1
        const cliente = clientes.shift()

        return (response.status(OK).json({cliente}))

    }
    catch (error) {
        throw (error)
    }

})



export default ClienteFilterRouter
