
import { Router } from 'express'

import FilterRouter     from './filter/ClienteFilterRouter'
import ListRouter       from './list/ClienteListRouter'
import RecommendRouter  from './recommend/ClienteRecommendRouter'
import SortRouter       from './sort/ClienteSortRouter'





const ClienteRouter = Router()

ClienteRouter.use('/filter',    FilterRouter)
ClienteRouter.use('/recommend', RecommendRouter)
ClienteRouter.use('/sort',      SortRouter)
ClienteRouter.use('',           ListRouter)



export default ClienteRouter
