
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import ClienteController        from '@controllers/Cliente/ClienteController'
import { ClienteInterface }     from '@entities/Cliente'
import EnricherParam            from '@enrichers/EnricherParam'
import SorterParam              from '@sorters/SorterParam'
import getComprasForClientes    from '@getters/Cliente/compras'





const ClienteSortRouter = Router()

/******************************************************************************
 *                      SORT Clientes - "GET /api/cliente/sort/compras"
 ******************************************************************************/
//  # 3 - Liste os clientes mais fiéis
ClienteSortRouter.get('/compras*', async (request: Request, response: Response) => 
{
    
    try {

        const clienteController = new ClienteController()
        const compras           = await getComprasForClientes()

        const enricherParams: EnricherParam[]   = [ { attribute: 'compras', entities: compras } ]
        const sorterParams:   SorterParam[]     = [ { attribute: 'compras' } ]

        const clientes = await clienteController.listAll(enricherParams, sorterParams)

        // const debugClientes = clientes.map((cliente: ClienteInterface) => (cliente!.compras?.length))
        // return (response.status(OK).json({debugClientes}))

        return (response.status(OK).json({clientes}))

    }
    catch (error) {
        throw (error)
    }

})



/******************************************************************************
 *                      SORT Clientes - "GET /api/cliente/sort/valortotal"
 ******************************************************************************/
//  # 1 - Liste os clientes ordenados pelo maior valor total em compras.
ClienteSortRouter.get('/valortotal*', async (request: Request, response: Response) => 
{
    
    try {

        const clienteController = new ClienteController()
        const compras           = await getComprasForClientes()

        const enricherParams: EnricherParam[]   = [ { attribute: 'compras', entities: compras } ]
        const sorterParams:   SorterParam[]     = [ { attribute: 'valorTotal' } ]

        const clientes = await clienteController.listAll(enricherParams, sorterParams)

        // const debugClientes = clientes.map((cliente: ClienteInterface) => (cliente.valorTotal))
        // return (response.status(OK).json({debugClientes}))

        return (response.status(OK).json({clientes}))

    }
    catch (error) {
        throw (error)
    }

})



export default ClienteSortRouter
