
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import HistoricoController  from '@controllers/Historico/HistoricoController'
import FilterParam          from '@filters/FilterParam'
import SorterParam          from '@sorters/SorterParam'





const CompraFilterRouter = Router()

/******************************************************************************
 *                      FILTER Historico por cliente - "GET /api/historico/filter/cliente/:cliente"
 ******************************************************************************/
CompraFilterRouter.get('/cliente/:cliente*', async (request: Request, response: Response) => 
{

    try {

        try {

            const historicoController = new HistoricoController()
    
            const filterParams: FilterParam[] = [ { attribute: 'cliente', value: request.params.cliente } ]
            const historicos                  = await historicoController.listAll(filterParams)
    
            return (response.status(OK).json({historicos}))
    
        }
        catch (error) {
            throw (error)
        }

    }
    catch (error) {
        throw (error)
    }

})



export default CompraFilterRouter
