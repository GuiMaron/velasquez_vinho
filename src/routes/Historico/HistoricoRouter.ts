
import { Router } from 'express'

import FilterRouter from './filter/HistoricoFilterRouter'
import ListRouter   from './list/HistoricoListRouter'





const HistoricoRouter = Router()

HistoricoRouter.use('/filter',  FilterRouter)
HistoricoRouter.use('',         ListRouter)



export default HistoricoRouter
