
import { OK }                           from 'http-status-codes'
import { Request, Response, Router }    from 'express'

import HistoricoController from '@controllers/Historico/HistoricoController'





const HistoricoListRouter = Router()


/******************************************************************************
 *                      LIST Historico - "GET /api/historico/list"
 ******************************************************************************/
HistoricoListRouter.get('*', async (request: Request, response: Response) => 
{
    
    try {

        const historicoController = new HistoricoController()
        const historicos          = await historicoController.listAll()

        return (response.status(OK).json({historicos}))

    }
    catch (error) {
        throw (error)
    }

})



export default HistoricoListRouter
