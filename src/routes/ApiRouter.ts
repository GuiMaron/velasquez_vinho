
import { Router } from 'express'

import ClienteRouter    from './Cliente/ClienteRouter'
import CompraRouter     from './Compra/CompraRouter'
import HistoricoRouter  from './Historico/HistoricoRouter'
import ItemRouter       from './Item/ItemRouter'





const router = Router()

router.use('/cliente',      ClienteRouter)
router.use('/compra',       CompraRouter)
router.use('/historico',    HistoricoRouter)
router.use('/item',         ItemRouter)



export default router
