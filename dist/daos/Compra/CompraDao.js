"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const MockDao_mock_1 = require("@daos/MockDb/MockDao.mock");
class CompraDao extends MockDao_mock_1.MockDaoMock {
    listAll() {
        const _super = Object.create(null, {
            listCompras: { get: () => super.listCompras }
        });
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const allCompras = yield _super.listCompras.call(this);
                return (allCompras);
            }
            catch (error) {
                throw (error);
            }
        });
    }
}
exports.default = CompraDao;
