"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MockDaoMock = void 0;
const tslib_1 = require("tslib");
const jsonfile_1 = tslib_1.__importDefault(require("jsonfile"));
class MockDaoMock {
    constructor() {
        this.clientesDbFilePath = `${((process.env.NODE_ENV === 'development') ? ('src') : ('.'))}/daos/MockDb/ClientesDb.json`;
        this.comprasDbFilePath = `${((process.env.NODE_ENV === 'development') ? ('src') : ('.'))}/daos/MockDb/ComprasDb.json`;
    }
    listClientes() {
        return (jsonfile_1.default.readFile(this.clientesDbFilePath));
    }
    listCompras() {
        return (jsonfile_1.default.readFile(this.comprasDbFilePath));
    }
}
exports.MockDaoMock = MockDaoMock;
