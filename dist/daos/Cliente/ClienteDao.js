"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ClienteDao = void 0;
const tslib_1 = require("tslib");
const MockDao_mock_1 = require("@daos/MockDb/MockDao.mock");
class ClienteDao extends MockDao_mock_1.MockDaoMock {
    listAll() {
        const _super = Object.create(null, {
            listClientes: { get: () => super.listClientes }
        });
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const clientes = yield _super.listClientes.call(this);
                return (clientes);
            }
            catch (error) {
                throw (error);
            }
        });
    }
}
exports.ClienteDao = ClienteDao;
exports.default = ClienteDao;
