"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adaptCompras = exports.adaptCompra = void 0;
const tslib_1 = require("tslib");
const Compra_1 = tslib_1.__importDefault(require("@entities/Compra"));
exports.adaptCompra = (data) => {
    return new Compra_1.default(`${data === null || data === void 0 ? void 0 : data.codigo}`, `${data === null || data === void 0 ? void 0 : data.data}`, `${data === null || data === void 0 ? void 0 : data.cliente}`, (data === null || data === void 0 ? void 0 : data.itens) || [], Number.parseFloat(`${data === null || data === void 0 ? void 0 : data.valorTotal}`));
};
exports.adaptCompras = (data) => {
    return data.map((tuple) => (exports.adaptCompra(tuple)));
};
