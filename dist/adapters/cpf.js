"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adaptCPF = void 0;
exports.adaptCPF = (cpf) => {
    return (`${cpf}`.replace(/(^0+|[^\d])/gi, ''));
};
