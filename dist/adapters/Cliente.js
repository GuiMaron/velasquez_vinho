"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adaptClientes = exports.adaptCliente = void 0;
const tslib_1 = require("tslib");
const Cliente_1 = tslib_1.__importDefault(require("@entities/Cliente"));
exports.adaptCliente = (data) => {
    return new Cliente_1.default(`${data === null || data === void 0 ? void 0 : data.nome}`, `${data === null || data === void 0 ? void 0 : data.cpf}`, data === null || data === void 0 ? void 0 : data.id, data === null || data === void 0 ? void 0 : data.compras, data === null || data === void 0 ? void 0 : data.valorTotal);
};
exports.adaptClientes = (data) => {
    return data.map((tuple) => (exports.adaptCliente(tuple)));
};
