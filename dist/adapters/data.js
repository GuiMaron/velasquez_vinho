"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.adaptData = void 0;
exports.adaptData = (data) => {
    let [day, month, year] = data.trim().split('-').map((fragment) => (Number.parseInt(`${fragment}`)));
    month--;
    return (new Date(year, month, day));
};
