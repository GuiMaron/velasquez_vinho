"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("@adapters/data");
const sortClienteByMaiorCompra = (clientes) => {
    const sortedClientes = clientes.slice(0);
    sortedClientes.sort((clienteA, clienteB) => {
        const [comprasA, comprasB] = [clienteA.compras || [], clienteB.compras || []];
        const [maiorCompraA, maiorCompraB] = [comprasA[0], comprasB[0]];
        const comprasDiff = (maiorCompraB.valorTotal - maiorCompraA.valorTotal);
        if (comprasDiff) {
            return (comprasDiff);
        }
        const [dataA, dataB] = [data_1.adaptData(maiorCompraA.data), data_1.adaptData(maiorCompraB.data)];
        const dataDiff = (+(dataB) - +(dataA));
        if (dataDiff) {
            return (dataDiff);
        }
        return (clienteA.nome.localeCompare(clienteB.nome));
    });
    return (sortedClientes);
};
exports.default = sortClienteByMaiorCompra;
