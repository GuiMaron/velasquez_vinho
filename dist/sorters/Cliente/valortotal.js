"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const sortClienteByValorTotal = (clientes) => {
    return (clientes.sort((clienteA, clienteB) => {
        const diff = (clienteB.valorTotal - clienteA.valorTotal);
        if (diff) {
            return (diff);
        }
        const [comprasA, comprasB] = [clienteA.compras || [], clienteB.compras || []];
        const comprasDiff = (comprasA.length - comprasB.length);
        if (comprasDiff) {
            return (comprasDiff);
        }
        return (clienteA.nome.localeCompare(clienteB.nome));
    }));
};
exports.default = sortClienteByValorTotal;
