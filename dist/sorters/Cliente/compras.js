"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("@adapters/data");
const sortClienteByCompras = (clientes) => {
    return (clientes.sort((clienteA, clienteB) => {
        const [comprasA, comprasB] = [clienteA.compras || [], clienteB.compras || []];
        const diff = (comprasB.length - comprasA.length);
        if (diff) {
            return (diff);
        }
        if ((!comprasA.length) && (!comprasA.length)) {
            return (clienteA.nome.localeCompare(clienteB.nome));
        }
        if (!comprasA.length) {
            return (-1);
        }
        if (!comprasB.length) {
            return (1);
        }
        const [firstCompraA, firstCompraB] = [comprasA[comprasA.length - 1], comprasB[comprasB.length - 1]];
        const [lastCompraA, lastCompraB] = [comprasA[0], comprasB[0]];
        const firstCompraDiff = (+(data_1.adaptData(firstCompraA.data)) - +(data_1.adaptData(firstCompraB.data)));
        if (firstCompraDiff) {
            return (firstCompraDiff);
        }
        const lastCompraDiff = (+(data_1.adaptData(lastCompraB.data)) - +(data_1.adaptData(lastCompraA.data)));
        if (lastCompraDiff) {
            return (lastCompraDiff);
        }
        return (clienteA.nome.localeCompare(clienteB.nome));
    }));
};
exports.default = sortClienteByCompras;
