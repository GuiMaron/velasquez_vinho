"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const data_1 = tslib_1.__importDefault(require("@sorters/Compra/data"));
const sortComprasByDataAno = (compras, desc = true) => {
    return (data_1.default(compras, desc, true));
};
exports.default = sortComprasByDataAno;
