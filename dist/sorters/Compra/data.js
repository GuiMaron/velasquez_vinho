"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("@adapters/data");
const sortComprasByData = (compras, desc = true, onlyAno = false) => {
    compras.sort((compraA, compraB) => {
        let [dataA, dataB] = [data_1.adaptData(compraA.data), data_1.adaptData(compraB.data)];
        if (onlyAno) {
            [dataA, dataB] = [new Date(dataA.getFullYear()), new Date(dataB.getFullYear())];
        }
        return (((desc)
            ? (+(dataB) - +(dataA))
            : (+(dataA) - +(dataB))));
    });
    return (compras);
};
exports.default = sortComprasByData;
