"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("@adapters/data");
const sortComprasByValorTotal = (compras, desc = true) => {
    compras.sort((compraA, compraB) => {
        const diff = ((desc)
            ? ((compraB.valorTotal - compraA.valorTotal))
            : ((compraA.valorTotal - compraB.valorTotal)));
        if (diff) {
            return (diff);
        }
        return ((desc)
            ? (+(data_1.adaptData(compraB.data)) - +(data_1.adaptData(compraA.data)))
            : (+(data_1.adaptData(compraA.data)) - +(data_1.adaptData(compraB.data))));
    });
    return (compras);
};
exports.default = sortComprasByValorTotal;
