"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.enrichClientesWithCompras = void 0;
const cpf_1 = require("@adapters/cpf");
const hashes_1 = require("@shared/hashes");
exports.enrichClientesWithCompras = (clientes, compras) => {
    const hashClientes = hashes_1.hashClientesByCPF(clientes);
    compras.map((compra) => {
        var _a;
        const cpf = cpf_1.adaptCPF(`${compra === null || compra === void 0 ? void 0 : compra.cliente}`);
        if (hashClientes[cpf]) {
            const cliente = hashClientes[cpf];
            (_a = cliente.compras) === null || _a === void 0 ? void 0 : _a.push(compra);
            cliente.valorTotal += compra.valorTotal;
        }
    });
    clientes.map((cliente) => {
        cliente.valorTotal = Number.parseFloat(Number(cliente.valorTotal).toFixed(2));
    });
    return (clientes);
};
