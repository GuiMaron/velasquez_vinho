"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hashClientesByCPF = void 0;
const cpf_1 = require("@adapters/cpf");
exports.hashClientesByCPF = (clientes) => {
    const hash = {};
    clientes.map((cliente) => (hash[cpf_1.adaptCPF(`${cliente === null || cliente === void 0 ? void 0 : cliente.cpf}`)] = cliente));
    return (hash);
};
