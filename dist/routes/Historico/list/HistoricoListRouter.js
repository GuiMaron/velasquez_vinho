"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const HistoricoController_1 = tslib_1.__importDefault(require("@controllers/Historico/HistoricoController"));
const HistoricoListRouter = express_1.Router();
HistoricoListRouter.get('*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const historicoController = new HistoricoController_1.default();
        const historicos = yield historicoController.listAll();
        return (response.status(http_status_codes_1.OK).json({ historicos }));
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = HistoricoListRouter;
