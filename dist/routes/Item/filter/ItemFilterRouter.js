"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const ItemController_1 = tslib_1.__importDefault(require("@controllers/Item/ItemController"));
const ItemFIlterRouter = express_1.Router();
ItemFIlterRouter.get('/:attribute/:value*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const itemController = new ItemController_1.default();
        const items = yield itemController.listAll([{ attribute: request.params.attribute, value: request.params.value }]);
        return (response.status(http_status_codes_1.OK).json({ items }));
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = ItemFIlterRouter;
