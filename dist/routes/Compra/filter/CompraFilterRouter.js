"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const CompraController_1 = tslib_1.__importDefault(require("@controllers/Compra/CompraController"));
const CompraFilterRouter = express_1.Router();
CompraFilterRouter.get('/data-ano/:ano*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        try {
            const compraController = new CompraController_1.default();
            const sorterParams = [{ attribute: 'valorTotal' }];
            const filterParams = [{ attribute: 'data-ano', value: request.params.ano }];
            const compras = yield compraController.listAll(sorterParams, filterParams);
            return (response.status(http_status_codes_1.OK).json({ compras }));
        }
        catch (error) {
            throw (error);
        }
    }
    catch (error) {
        throw (error);
    }
}));
CompraFilterRouter.get('/cliente/:cliente*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        try {
            const compraController = new CompraController_1.default();
            const filterParams = [{ attribute: 'cliente', value: request.params.cliente }];
            const compras = yield compraController.listAll(undefined, filterParams);
            return (response.status(http_status_codes_1.OK).json({ compras }));
        }
        catch (error) {
            throw (error);
        }
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = CompraFilterRouter;
