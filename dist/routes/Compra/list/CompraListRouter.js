"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const CompraController_1 = tslib_1.__importDefault(require("@controllers/Compra/CompraController"));
const CompraListRouter = express_1.Router();
CompraListRouter.get('*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const compraController = new CompraController_1.default();
        const compras = yield compraController.listAll();
        return (response.status(http_status_codes_1.OK).json({ compras }));
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = CompraListRouter;
