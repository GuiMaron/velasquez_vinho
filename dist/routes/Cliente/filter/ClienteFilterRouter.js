"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const ClienteController_1 = tslib_1.__importDefault(require("@controllers/Cliente/ClienteController"));
const maiorCompraFrom_1 = tslib_1.__importDefault(require("@filters/Cliente/maiorCompraFrom"));
const compras_1 = require("@getters/Cliente/compras");
const maiorCompra_1 = tslib_1.__importDefault(require("@sorters/Cliente/maiorCompra"));
const ClienteFilterRouter = express_1.Router();
ClienteFilterRouter.get('/maior-compra-ano/:ano*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const clienteController = new ClienteController_1.default();
        const compras = yield compras_1.getComprasFromAnoForClientes(request.params.ano);
        const enricherParams = [{ attribute: 'compras', entities: compras }];
        const sorterParams = [{ attribute: 'valorTotal' }];
        const filterParams = [{ attribute: 'compras' }];
        let clientes = yield clienteController.listAll(enricherParams, sorterParams, filterParams);
        clientes = maiorCompraFrom_1.default(clientes);
        clientes = maiorCompra_1.default(clientes);
        const cliente = clientes.shift();
        return (response.status(http_status_codes_1.OK).json({ cliente }));
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = ClienteFilterRouter;
