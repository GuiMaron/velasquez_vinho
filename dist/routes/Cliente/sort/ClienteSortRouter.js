"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const ClienteController_1 = tslib_1.__importDefault(require("@controllers/Cliente/ClienteController"));
const compras_1 = tslib_1.__importDefault(require("@getters/Cliente/compras"));
const ClienteSortRouter = express_1.Router();
ClienteSortRouter.get('/compras*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const clienteController = new ClienteController_1.default();
        const compras = yield compras_1.default();
        const enricherParams = [{ attribute: 'compras', entities: compras }];
        const sorterParams = [{ attribute: 'compras' }];
        const clientes = yield clienteController.listAll(enricherParams, sorterParams);
        return (response.status(http_status_codes_1.OK).json({ clientes }));
    }
    catch (error) {
        throw (error);
    }
}));
ClienteSortRouter.get('/valortotal*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const clienteController = new ClienteController_1.default();
        const compras = yield compras_1.default();
        const enricherParams = [{ attribute: 'compras', entities: compras }];
        const sorterParams = [{ attribute: 'valorTotal' }];
        const clientes = yield clienteController.listAll(enricherParams, sorterParams);
        return (response.status(http_status_codes_1.OK).json({ clientes }));
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = ClienteSortRouter;
