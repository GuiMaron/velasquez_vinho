"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const RecommendationController_1 = tslib_1.__importDefault(require("@controllers/Recommendation/RecommendationController"));
const ClienteRecommendRouter = express_1.Router();
ClienteRecommendRouter.get('/:cliente/:type*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const recommendationController = new RecommendationController_1.default();
        const recommendation = yield recommendationController.recommend(request.params.cliente, request.params.type);
        return (response.status(http_status_codes_1.OK).json({ recommendation }));
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = ClienteRecommendRouter;
