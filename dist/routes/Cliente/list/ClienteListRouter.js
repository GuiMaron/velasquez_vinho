"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const http_status_codes_1 = require("http-status-codes");
const express_1 = require("express");
const ClienteController_1 = tslib_1.__importDefault(require("@controllers/Cliente/ClienteController"));
const ClienteListRouter = express_1.Router();
ClienteListRouter.get('*', (request, response) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const clienteController = new ClienteController_1.default();
        const clientes = yield clienteController.listAll();
        return (response.status(http_status_codes_1.OK).json({ clientes }));
    }
    catch (error) {
        throw (error);
    }
}));
exports.default = ClienteListRouter;
