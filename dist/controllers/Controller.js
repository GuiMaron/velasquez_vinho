"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const splitEntitities_1 = tslib_1.__importDefault(require("@entities/splitEntitities"));
class Controller {
    constructor() {
        this.ENRICHABLE_BY = {};
        this.FILTERABLE_BY = {};
        this.SORTABLE_BY = {};
    }
    enrich(entities, params) {
        if (params.length) {
            const param = params.shift();
            if (!this.ENRICHABLE_BY[param.attribute]) {
                throw (new Error(`Invalid enricher param: ${param.attribute}`));
            }
            entities = this.ENRICHABLE_BY[param.attribute].call(null, entities, param.entities);
        }
        return entities;
    }
    filter(entities, params) {
        try {
            if (params.length) {
                const param = params.shift();
                if (!this.FILTERABLE_BY[param.attribute]) {
                    throw (new Error(`Invalid filter param: ${param.attribute}`));
                }
                entities = this.FILTERABLE_BY[param.attribute].call(null, entities, param.value);
                let splittedEntitites = splitEntitities_1.default(entities, param.attribute);
                splittedEntitites = splittedEntitites.map((split) => (this.sort(split.slice(0), params.slice(0))));
                entities = [];
                splittedEntitites.map((orderedSplit) => (entities = entities.concat(orderedSplit)));
            }
            return entities;
        }
        catch (error) {
            throw (error);
        }
    }
    sort(entities, params) {
        try {
            if (params.length) {
                const param = params.shift();
                if (!this.SORTABLE_BY[param.attribute]) {
                    throw (new Error(`Invalid sort param: ${param.attribute}`));
                }
                entities = this.SORTABLE_BY[param.attribute].call(null, entities, param.desc);
                let splittedEntitites = splitEntitities_1.default(entities, param.attribute);
                splittedEntitites = splittedEntitites.map((split) => (this.sort(split.slice(0), params.slice(0))));
                entities = [];
                splittedEntitites.map((orderedSplit) => (entities = entities.concat(orderedSplit)));
            }
            return entities;
        }
        catch (error) {
            throw (error);
        }
    }
}
exports.default = Controller;
