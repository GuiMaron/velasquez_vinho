"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const cpf_1 = require("@adapters/cpf");
const data_1 = require("@adapters/data");
const Controller_1 = tslib_1.__importDefault(require("@controllers/Controller"));
const cliente_1 = tslib_1.__importDefault(require("@filters/Historico/cliente"));
const Historico_1 = tslib_1.__importStar(require("@entities/Historico"));
const CompraDao = require(`@daos/Compra/CompraDao${((process.env.NODE_ENV === 'development') ? ('.mock') : (''))}`).default;
class HistoricoController extends Controller_1.default {
    constructor() {
        super(...arguments);
        this.FILTERABLE_BY = {
            'cliente': cliente_1.default
        };
    }
    listAll(filterBy) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const compraDao = new CompraDao();
                const compras = yield compraDao.listAll();
                const historicosHash = {};
                compras.map((compra) => {
                    const cliente = cpf_1.adaptCPF(compra.cliente);
                    if (!historicosHash[cliente]) {
                        historicosHash[cliente] = new Historico_1.default(cliente);
                    }
                    const historico = historicosHash[cliente];
                    compra.itens.map((item) => {
                        if (item.codigo) {
                            Historico_1.addHistorico(item, historico, 'produto', 'codigo', item.codigo, { produto: item.produto });
                        }
                        else {
                            Historico_1.addHistorico(item, historico, 'produto', 'produto', item.produto, { codigo: null });
                        }
                        if (item.variedade) {
                            Historico_1.addHistorico(item, historico, 'variedade', 'variedade', item.variedade);
                        }
                        if (item.pais) {
                            Historico_1.addHistorico(item, historico, 'pais', 'pais', item.pais);
                        }
                        if (item.categoria) {
                            Historico_1.addHistorico(item, historico, 'categoria', 'categoria', item.categoria);
                        }
                        if (item.safra) {
                            Historico_1.addHistorico(item, historico, 'safra', 'safra', item.safra);
                        }
                    });
                    if (!historico.ultimaCompra) {
                        historico.ultimaCompra = compra;
                    }
                    if (+(data_1.adaptData(historico.ultimaCompra.data)) < +(data_1.adaptData(compra.data))) {
                        historico.ultimaCompra = compra;
                    }
                });
                let historicos = Historico_1.sortHistoricos(Object.values(historicosHash));
                if (filterBy) {
                    historicos = this.filter(historicos, filterBy);
                }
                return (historicos);
            }
            catch (error) {
                throw (error);
            }
        });
    }
}
exports.default = HistoricoController;
