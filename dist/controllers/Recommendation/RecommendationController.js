"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Controller_1 = tslib_1.__importDefault(require("@controllers/Controller"));
const HistoricoController_1 = tslib_1.__importDefault(require("@controllers/Historico/HistoricoController"));
const ItemController_1 = tslib_1.__importDefault(require("@controllers/Item/ItemController"));
class RecommendationController extends Controller_1.default {
    listAll() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const itemController = new ItemController_1.default();
            const items = yield itemController.listAll();
            return (items);
        });
    }
    recommend(cliente, type) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            const historicoController = new HistoricoController_1.default();
            const itemController = new ItemController_1.default();
            const filterParams = [{ attribute: 'cliente', value: cliente }];
            const historico = yield (yield historicoController.listAll(filterParams)).shift();
            const topInType = historico[type][0];
            const topValue = topInType[type];
            const item = yield (yield itemController.listAll([{ attribute: type, value: topValue }])).shift();
            return item;
        });
    }
}
exports.default = RecommendationController;
