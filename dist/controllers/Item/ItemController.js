"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Controller_1 = tslib_1.__importDefault(require("@controllers/Controller"));
const item_1 = require("@filters/Item/item");
const Item_1 = require("@entities/Item");
const CompraDao = require(`@daos/Compra/CompraDao${((process.env.NODE_ENV === 'development') ? ('.mock') : (''))}`).default;
class ItemController extends Controller_1.default {
    constructor() {
        super(...arguments);
        this.FILTERABLE_BY = {
            'produto': item_1.filterProdutosByProduto,
            'variedade': item_1.filterProdutosByVariedade,
            'pais': item_1.filterProdutosByPais,
            'categoria': item_1.filterProdutosByCategoria,
            'safra': item_1.filterProdutosBySafra,
            'preco': item_1.filterProdutosByPreco
        };
    }
    listAll(filterBy) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const compraDao = new CompraDao();
                const compras = yield compraDao.listAll();
                let items = [];
                compras.map((compra) => {
                    compra.itens.map((item) => {
                        let index = Item_1.searchItem(item, items);
                        if (-1 === index) {
                            items.push(item);
                        }
                        else {
                            if ((!items[index].codigo) && (item.codigo)) {
                                items[index].codigo = item.codigo;
                            }
                        }
                    });
                });
                if (filterBy) {
                    items = this.filter(items, filterBy);
                }
                return (items);
            }
            catch (error) {
                throw (error);
            }
        });
    }
}
exports.default = ItemController;
