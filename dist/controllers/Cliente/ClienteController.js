"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Cliente_1 = require("@adapters/Cliente");
const Controller_1 = tslib_1.__importDefault(require("@controllers/Controller"));
const Cliente_2 = require("@enrichers/Cliente");
const withCompras_1 = tslib_1.__importDefault(require("@filters/Cliente/withCompras"));
const valortotal_1 = tslib_1.__importDefault(require("@sorters/Cliente/valortotal"));
const compras_1 = tslib_1.__importDefault(require("@sorters/Cliente/compras"));
const ClienteDao = require(`@daos/Cliente/ClienteDao${((process.env.NODE_ENV === 'development') ? ('.mock') : (''))}`).default;
class ClienteController extends Controller_1.default {
    constructor() {
        super(...arguments);
        this.ENRICHABLE_BY = {
            'compras': Cliente_2.enrichClientesWithCompras
        };
        this.FILTERABLE_BY = {
            'compras': withCompras_1.default
        };
        this.SORTABLE_BY = {
            'compras': compras_1.default,
            'valorTotal': valortotal_1.default
        };
    }
    listAll(enrichedBy, sortBy, filterBy) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const clienteDao = new ClienteDao();
                let clientes = yield clienteDao.listAll();
                clientes = Cliente_1.adaptClientes(clientes);
                if (enrichedBy) {
                    clientes = this.enrich(clientes, enrichedBy);
                }
                if (sortBy) {
                    clientes = this.sort(clientes, sortBy);
                }
                if (filterBy) {
                    clientes = this.filter(clientes, filterBy);
                }
                return (clientes);
            }
            catch (error) {
                throw (error);
            }
        });
    }
}
exports.default = ClienteController;
