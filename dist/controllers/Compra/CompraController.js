"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const Compra_1 = require("@adapters/Compra");
const Controller_1 = tslib_1.__importDefault(require("@controllers/Controller"));
const cliente_1 = tslib_1.__importDefault(require("@filters/Compra/cliente"));
const inAno_1 = tslib_1.__importDefault(require("@filters/Compra/inAno"));
const data_1 = tslib_1.__importDefault(require("@sorters/Compra/data"));
const dataAno_1 = tslib_1.__importDefault(require("@sorters/Compra/dataAno"));
const valorTotal_1 = tslib_1.__importDefault(require("@sorters/Compra/valorTotal"));
const CompraDao = require(`@daos/Compra/CompraDao${((process.env.NODE_ENV === 'development') ? ('.mock') : (''))}`).default;
class CompraController extends Controller_1.default {
    constructor() {
        super(...arguments);
        this.FILTERABLE_BY = {
            'cliente': cliente_1.default,
            'data-ano': inAno_1.default
        };
        this.SORTABLE_BY = {
            'data': data_1.default,
            'data-ano': dataAno_1.default,
            'valorTotal': valorTotal_1.default
        };
    }
    listAll(sortBy, filterBy) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                const compraDao = new CompraDao();
                let compras = yield compraDao.listAll();
                compras = Compra_1.adaptCompras(compras);
                if (sortBy) {
                    compras = this.sort(compras, sortBy);
                }
                if (filterBy) {
                    compras = this.filter(compras, filterBy);
                }
                return (compras);
            }
            catch (error) {
                throw (error);
            }
        });
    }
}
exports.default = CompraController;
