"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cpf_1 = require("@adapters/cpf");
const filterComprasByCliente = (compras, cliente) => {
    return (compras.filter((compra) => ((cpf_1.adaptCPF(compra.cliente) === cpf_1.adaptCPF(cliente)))));
};
exports.default = filterComprasByCliente;
