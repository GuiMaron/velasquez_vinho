"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("@adapters/data");
const filterComprasInAno = (compras, year) => {
    return (compras.filter((compra) => ((`${data_1.adaptData(compra.data).getFullYear()}` === year))));
};
exports.default = filterComprasInAno;
