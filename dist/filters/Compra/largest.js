"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const data_1 = require("@adapters/data");
const filterLargestCompra = (clientes, year) => {
    return (clientes.filter((cliente) => {
        const compras = cliente.compras || [];
        const hasCompraInYear = compras.reduce((hasCompra, compra) => (hasCompra || (`${data_1.adaptData(compra.data).getFullYear()}` === year)), false);
        return (hasCompraInYear);
    }));
};
exports.default = filterLargestCompra;
