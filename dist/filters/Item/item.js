"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.filterProdutosByVariedade = exports.filterProdutosBySafra = exports.filterProdutosByProduto = exports.filterProdutosByPreco = exports.filterProdutosByPais = exports.filterProdutosByCategoria = void 0;
const filterItemByAttribute = (items, value, attribute) => {
    return (items.filter((item) => item[attribute] == value));
};
exports.filterProdutosByCategoria = (items, value) => {
    return (filterItemByAttribute(items, value, 'categoria'));
};
exports.filterProdutosByPais = (items, value) => {
    return (filterItemByAttribute(items, value, 'pais'));
};
exports.filterProdutosByPreco = (items, value) => {
    return (filterItemByAttribute(items, value, 'preco'));
};
exports.filterProdutosByProduto = (items, value) => {
    return (filterItemByAttribute(items, value, 'produto'));
};
exports.filterProdutosBySafra = (items, value) => {
    return (filterItemByAttribute(items, value, 'safra'));
};
exports.filterProdutosByVariedade = (items, value) => {
    return (filterItemByAttribute(items, value, 'variedade'));
};
