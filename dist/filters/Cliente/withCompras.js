"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const filterClienteswithCompras = (clientes) => {
    return (clientes.filter((cliente) => {
        const compras = cliente.compras || [];
        return (compras.length);
    }));
};
exports.default = filterClienteswithCompras;
