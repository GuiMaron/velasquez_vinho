"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const filterMaiorCompraFromCliente = (clientes) => {
    return (clientes.map((cliente) => {
        const compras = cliente.compras || [];
        cliente.compras = [compras[0]] || [];
        return (cliente);
    }));
};
exports.default = filterMaiorCompraFromCliente;
