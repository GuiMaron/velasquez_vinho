"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cpf_1 = require("@adapters/cpf");
const filterHistoricoByCliente = (historicos, cliente) => {
    return (historicos.filter((historico) => (historico.cliente === cpf_1.adaptCPF(cliente))));
};
exports.default = filterHistoricoByCliente;
