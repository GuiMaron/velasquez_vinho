"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getComprasFromAnoForClientes = exports.getComprasForClientes = void 0;
const tslib_1 = require("tslib");
const CompraController_1 = tslib_1.__importDefault(require("@controllers/Compra/CompraController"));
exports.getComprasForClientes = (sortBy, filterBy) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    try {
        const comprasController = new CompraController_1.default();
        const sorterParams = sortBy || [{ attribute: 'data' }];
        const compras = yield comprasController.listAll(sorterParams, filterBy);
        return (compras);
    }
    catch (error) {
        throw (error);
    }
});
exports.getComprasFromAnoForClientes = (ano, sortBy, filterBy) => tslib_1.__awaiter(void 0, void 0, void 0, function* () {
    if (!ano) {
        ano = `${new Date().getFullYear()}`;
    }
    try {
        const comprasController = new CompraController_1.default();
        const sorterParams = [{ attribute: 'data-ano' }, { attribute: 'valorTotal' }];
        const filterParams = [{ attribute: 'data-ano', value: ano }];
        const compras = yield comprasController.listAll(sorterParams, filterParams);
        return (compras);
    }
    catch (error) {
        throw (error);
    }
});
exports.default = exports.getComprasForClientes;
