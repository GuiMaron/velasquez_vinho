"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
require("./LoadEnv");
if ('development' != process.env.NODE_ENV) {
    require('module-alias/register');
}
const _server_1 = tslib_1.__importDefault(require("@server"));
const Logger_1 = tslib_1.__importDefault(require("@shared/Logger"));
const defaults_1 = tslib_1.__importDefault(require("./defaults"));
const port = Number(process.env.PORT || defaults_1.default.PORT);
_server_1.default.listen(port, () => {
    Logger_1.default.info('Express server started on port: ' + port);
});
