"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const splitEntitites = (tuples, attribute) => {
    const splits = [];
    if (!tuples) {
        return (splits);
    }
    tuples.map((tuple, index) => {
        if ((index === 0)
            || (tuple[attribute] !== tuples[index - 1][attribute])) {
            splits[splits.length] = [];
        }
        splits[splits.length - 1].push(tuple);
    });
    return splits;
};
exports.default = splitEntitites;
