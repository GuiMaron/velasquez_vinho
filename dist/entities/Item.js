"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.searchItem = exports.compareItems = void 0;
class Item {
    constructor(produtoOuItem, codigo, variedade, pais, categoria, safra, preco) {
        if ('string' === typeof (produtoOuItem)) {
            this.produto = `${produtoOuItem}`;
            this.codigo = `${codigo}`;
            this.variedade = `${variedade}`;
            this.pais = `${pais}`;
            this.categoria = `${categoria}`;
            this.safra = `${safra}`;
            this.preco = Number.parseFloat(`${preco}`);
        }
        else {
            this.produto = produtoOuItem.produto;
            this.codigo = produtoOuItem.codigo;
            this.variedade = produtoOuItem.variedade;
            this.pais = produtoOuItem.pais;
            this.categoria = produtoOuItem.categoria;
            this.safra = produtoOuItem.safra;
            this.preco = produtoOuItem.preco;
        }
    }
}
exports.compareItems = (itemA, itemB) => {
    return ((itemA.produto === itemB.produto)
        && (itemA.variedade === itemB.variedade)
        && (itemA.pais === itemB.pais)
        && (itemA.categoria === itemB.categoria)
        && (itemA.safra === itemB.safra));
};
exports.searchItem = (item, items) => {
    let index = -1;
    for (let index in items) {
        const currentItem = items[index];
        if (exports.compareItems(currentItem, item)) {
            return (Number.parseInt(index));
        }
    }
    return (index);
};
exports.default = Item;
