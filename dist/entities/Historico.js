"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sortHistoricos = exports.sortHistorico = exports.searchHistorico = exports.addHistorico = void 0;
class Historico {
    constructor(clienteOuHistorico, produto = [], variedade = [], pais = [], categoria = [], safra = [], ultimaCompra = null) {
        if ('string' === typeof (clienteOuHistorico)) {
            this.cliente = `${clienteOuHistorico}`;
            this.produto = produto;
            this.variedade = variedade;
            this.pais = pais;
            this.categoria = categoria;
            this.safra = safra;
            this.ultimaCompra = ultimaCompra;
        }
        else {
            this.cliente = clienteOuHistorico.cliente;
            this.produto = clienteOuHistorico.produto;
            this.variedade = clienteOuHistorico.variedade;
            this.pais = clienteOuHistorico.pais;
            this.categoria = clienteOuHistorico.categoria;
            this.safra = clienteOuHistorico.safra;
            this.ultimaCompra = clienteOuHistorico.ultimaCompra;
        }
    }
}
exports.addHistorico = (item, historico, type, attribute, value, extraData) => {
    let index = exports.searchHistorico(historico, type, attribute, item[attribute]);
    if (-1 === index) {
        let tuple = { quantidade: 1 };
        tuple[attribute] = value;
        if (extraData) {
            tuple = Object.assign(tuple, extraData);
        }
        historico[type].push(tuple);
    }
    else {
        historico[type][index].quantidade++;
    }
};
exports.searchHistorico = (historico, type, attribute, value) => {
    let index = -1;
    if (!historico[type]) {
        return (index);
    }
    const tuples = historico[type];
    for (let index in tuples) {
        let tuple = tuples[index];
        if (tuple[attribute] === value) {
            return (Number.parseInt(index));
        }
    }
    return (index);
};
exports.sortHistorico = (historico) => {
    Object.keys(historico).map((type) => {
        historico[type] = sortTipoHistorico(historico[type]);
    });
    return (historico);
};
exports.sortHistoricos = (historicos) => {
    return (historicos.map((historico) => exports.sortHistorico(historico)));
};
const sortTipoHistorico = (tipoHistorico) => {
    if (!Array.isArray(tipoHistorico)) {
        return (tipoHistorico);
    }
    tipoHistorico.sort((itemA, itemB) => (itemB.quantidade - itemB.quantidade));
    return (tipoHistorico);
};
exports.default = Historico;
