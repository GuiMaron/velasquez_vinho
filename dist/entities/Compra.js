"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Compra {
    constructor(codigoOuCompra, data, cliente, itens = [], valorTotal = NaN) {
        var _a;
        if ('string' === typeof (codigoOuCompra)) {
            [
                this.codigo,
                this.data,
                this.cliente,
                this.itens,
                this.valorTotal
            ]
                =
                    [
                        `${codigoOuCompra}`,
                        `${data}`,
                        `${(('string' !== typeof (cliente)) ? (`${(_a = cliente) === null || _a === void 0 ? void 0 : _a.nome}`) : (`${cliente}`))}`,
                        itens,
                        Number.parseFloat(`${valorTotal}`)
                    ];
        }
        else {
            [
                this.codigo,
                this.data,
                this.cliente,
                this.itens,
                this.valorTotal
            ]
                =
                    [
                        codigoOuCompra.codigo,
                        codigoOuCompra.data,
                        codigoOuCompra.cliente,
                        codigoOuCompra.itens,
                        codigoOuCompra.valorTotal
                    ];
        }
    }
}
exports.default = Compra;
