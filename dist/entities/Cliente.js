"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cliente {
    constructor(nomeOuCliente, cpf = '', id = -1, compras = [], valorTotal = 0) {
        this.compras = [];
        if ('string' === typeof (nomeOuCliente)) {
            this.compras = compras;
            this.cpf = `${cpf}`;
            this.id = id;
            this.nome = `${nomeOuCliente}`;
            this.valorTotal = valorTotal;
        }
        else {
            this.compras = nomeOuCliente.compras;
            this.cpf = nomeOuCliente.cpf;
            this.id = nomeOuCliente.id;
            this.nome = nomeOuCliente.nome;
            this.valorTotal = nomeOuCliente.valorTotal;
        }
    }
}
exports.default = Cliente;
