
import Cliente from '@entities/Cliente'



const clientes = [
    new Cliente("Vinicius", "000.000.000-01", 1)
,   new Cliente("Marcos",   "000.000.000-02", 2)
,   new Cliente("Joel",     "000.000.000-03", 3)
,   new Cliente("Gustavo",  "000.000.000-04", 4)
,   new Cliente("Raquel",   "000.000.000-05", 5)
,   new Cliente("Pamela",   "000.000.000-06", 6)
,   new Cliente("Bruno",    "000.000.000-07", 7)
,   new Cliente("Jonathan", "000.000.000-08", 8)
,   new Cliente("Matheus",  "000.000.000-09", 9)
,   new Cliente("Rafael",   "000.000.000-10", 10)
]

export default clientes
