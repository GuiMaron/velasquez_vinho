
import supertest                        from 'supertest'
import { BAD_REQUEST, OK }              from 'http-status-codes'
import { Response, SuperTest, Test }    from 'supertest'

import app                              from '@server'
import Cliente, { ClienteInterface }    from '@entities/Cliente'
import ClienteController                from '@controllers/Cliente/ClienteController'
import { pErr }                         from '@shared/functions'

//  Dados de teste
const clientes = require('../../../../data/clientes').default



describe('Rotas: API / Cliente / list', () => 
{

    const basePath          = '/api/cliente/list'

    const listClientesPath  = `${basePath}`


    let agent: SuperTest<Test>

    beforeAll((done) => {
        agent = supertest.agent(app)
        done()
    })



    describe(`"GET:${listClientesPath}"`, () => 
    {

        it(
            `deve retornar um json com a lista de todos os clientes e status code "${OK}"`
        ,   (done) => 
        {

            // spyOn(ClienteController.prototype, 'listAll').and.returnValue(Promise.resolve(clientes))

            agent
            .get(listClientesPath)
            .end((err: Error, res: Response) => 
            {

                pErr(err)
                expect(res.status).toBe(OK)

                // Caste instance-objects to 'Cliente' objects
                const returnClientes = res.body.clientes.map((cliente: ClienteInterface) => (new Cliente(cliente)))

                expect(returnClientes).toEqual(clientes)
                expect(res.body.error).toBeUndefined()
                done()

            })

        })



        // it(
        //     `deve retornar um json com uma mensagem de erro e status code "${BAD_REQUEST}" se a chamada falhar`
        // ,   (done) => 
        // {

        //     const errMsg = 'Could not fetch Clientes.'
        //     spyOn(ClienteController.prototype, 'listAll').and.throwError(errMsg)

        //     agent
        //     .get(listClientesPath)
        //     .end((error: Error, res: Response) => 
        //     {

        //         pErr(error)
        //         expect(res.status).toBe(BAD_REQUEST)
        //         expect(res.body.error).toBe(errMsg)
        //         done()

        //     })

        // })

    })

})
