
import supertest                        from 'supertest'
import { BAD_REQUEST, OK }              from 'http-status-codes'
import { Response, SuperTest, Test }    from 'supertest'

import app                              from '@server'
import Cliente, { ClienteInterface }    from '@entities/Cliente'
import ClienteController                from '@controllers/Cliente/ClienteController'
import { pErr }                         from '@shared/functions'

//  Dados de teste
const clientes = require('../../../../data/clientes').default



describe('Rotas: API / Cliente / sort / valortotal', () => 
{

    const basePath          = '/api/cliente/sort/valortotal'

    const sortClientesPath  = `${basePath}`


    let agent: SuperTest<Test>

    beforeAll((done) => {
        agent = supertest.agent(app)
        done()
    })



    describe(`"GET:${sortClientesPath}"`, () => 
    {

        it(
            `deve retornar um json com a lista de todos os clientes ordenados pelo valorTotal de suas compras e status code "${OK}"`
        ,   (done) => 
        {

            // spyOn(ClienteController.prototype, 'sortValorTotal').and.returnValue(Promise.resolve(clientes))

            agent
            .get(sortClientesPath)
            .end((err: Error, res: Response) => 
            {

                pErr(err)
                expect(res.status).toBe(OK)

                // Caste instance-objects to 'Cliente' objects
                const returnClientes = res.body.clientes.map((cliente: ClienteInterface) => (new Cliente(cliente)))

                //  Apenas sem erros
                expect(res.body.error).toBeUndefined()
                
                //  Testar se realmente estão em ordem
                const ordered = returnClientes.reduce((ordered: boolean, cliente: ClienteInterface, index: number) => 
                    (ordered && ((index === 0) || (cliente.valorTotal <= returnClientes[index - 1].valorTotal)))
                ,   true)

                if (! ordered) {
                    fail('Not ordered')
                }
    
                done()

            })

        })

    })

})
